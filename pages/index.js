import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import { useUser } from "@auth0/nextjs-auth0";
import JSONPretty from 'react-json-pretty';

export default function Home() {
  const { user, error, isLoading } = useUser();

  return (
    <div className={styles.container}>
      <Head>
        <title>Auth0 Experiment</title>
        <meta name="description" content="Using Auth0 in Next.js" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <nav className={styles.nav}>
        <div>
          <a>Hello World Inc.</a>
        </div>
        <div>
          { user ? <>
            <a href="/api/auth/logout">Logout</a>
            <img src={user.picture}/>
          </> : <>
            <a href="/api/auth/login">Login</a>
          </>}
        </div>
      </nav>

      <main className={styles.main}>
        { 
          isLoading  && <span>Loading...</span>                         ||
          error      && <span>Something went wrong with loading</span>  ||
          !user      && <span>You are currently not logged in</span>    || <>
            <span>
              Hello {user.given_name} you are currently logged in as {user.email}
            </span>
            <JSONPretty data={user}/>
          </>
        }
      </main>
    </div>
  )
}
